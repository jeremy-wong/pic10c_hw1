#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

// Global constants (if any)
const int INITIAL_MONEY = 100;
const int MAX_TO_LOSE = 900;

// Non member functions declarations (if any)
int player_turn(Hand& h1, Player& p1);
void dealer_turn(Hand& h2);
bool winning_condition(const Hand& h1, const Hand& h2);

// Non member functions implementations (if any)
int player_turn(Hand& h1, Player& p1) {
	bool flag = true;
	string s_flag;
	int bets;

	cout << "You have $" << p1.get_money() << ". Enter bet: ";
	cin >> bets;

	if (cin.fail()) {
		cout << "Error\n";
		return 0;
	}
	else if (bets > p1.get_money()) {
		cout << "Not enough money!\n";
		return 0;
	}

	while (h1.get_total() < 7.5 && flag) {
		cout << "\nYour cards:\n" << h1.display_hand()
			<< "Your total is " << h1.get_total()
			<< ". Do you want another card(y/n)? ";

		cin >> s_flag;
		if (cin.fail()) {
			cout << "Error\n";
			flag = false;
		}
		else if (s_flag == "n" || s_flag == "N") {
			flag = false;
		}
		else if (s_flag == "y" || s_flag == "Y") {
			h1.add_card();
		}
		else {
			cout << "Wrong input!\n";
			flag = false;
		}
	}
	return bets;
}

void dealer_turn(Hand& h2) {
	cout << "\nDealer's cards:\n" << h2.display_hand()
		<< "The dealer's total is " << h2.get_total() << ".\n";
	while (h2.get_total() < 5.5)
	{
		h2.add_card();
		cout << "\nDealer's cards:\n" << h2.display_hand()
			<< "The dealer's total is " << h2.get_total() << ".\n";
	}
}

// Stub for main
int main(){
   /* --STATEMENTS-- */
	ofstream myfile;
	myfile.open("companion_log.txt");


	Player p1(INITIAL_MONEY);
	Player dealer(MAX_TO_LOSE);
	int bets;
	int round = 1;
	do {
		Hand player_hand, dealer_hand;
		bets = player_turn(player_hand, p1);
		dealer_turn(dealer_hand);

		myfile << "-----------------------------------------------\n\n"
			<< "Gane number: " << round << "\t Money left: $" << p1.get_money()
			<< "\nBet: " << bets << "\n\n Your cards:\n" 
			<< player_hand.display_hand() << "Your total: " 
			<< player_hand.get_total() << ".\n\n Dealer's cards:\n"
			<< dealer_hand.display_hand() << "Dealer's total is " 
			<< dealer_hand.get_total() << ".\n\n";
		
		round++;
		if (player_hand.get_total() > 7.5) {
			cout << p1.bet(bets, false);
		}
		else if (dealer_hand.get_total() > 7.5 
			|| (7.5 - player_hand.get_total()) 
			< (7.5 - dealer_hand.get_total())) {
			 cout << p1.bet(bets, true);
			 dealer.bet(bets, false);
		}
		else if ((7.5 - player_hand.get_total())
			> (7.5 - dealer_hand.get_total())) {
			 cout << p1.bet(bets, false);
		}
	} while (p1.get_money() > 0 && dealer.get_money() > 0);
	myfile << "-----------------------------------------------\n\n";
	myfile.close();
   return 0;
}
