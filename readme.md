This is a single-player version of siete y medio.

---Game flow

The player first place a bet, the player and the dealer then draw a card.
After that, the player is repeatedly asked to draw another card or not.
When the player stops to draw another card or busted, the dealer will draw cards until the total exceed 5.5 or busted.
When both the player and the dealer stop drawing cards, their hand will be compared and see who win the game.

---Winning condition

If the player comes closer to 7 and a half than the dealer, or the dealer is busted while the player isn't, the player wins.
The player will win the round and the player's money would increase by the amount of the bet.
If the dealer comes closer to 7 and a half than the player, or the player is busted, the dealer wins.
The player will lose the round and the player's money would decrease by the amount of the bet.
If both the player and dealer are not busted and having the same total, a tie is declared and nothing happen to the player's money.

---Ending condition

Player will start with 100 dollars, and the game will end whenever the player loses all the money or the dealer loses more than 900 dollars.

Have fun!